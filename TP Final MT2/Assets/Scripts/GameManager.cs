using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public int defeatedEnemies;
    // Start is called before the first frame update
    void Start()
    {
        defeatedEnemies = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(1) && Input.GetMouseButtonDown(0))
        {
            GestorDeAudio.instancia.ReproducirSonido("Disparo");
        }
    }
}
