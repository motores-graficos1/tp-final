using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bala : MonoBehaviour
{
    private Rigidbody rb;
    public float speed;
    public LayerMask capasIgnoradas;
    // Start is called before the first frame update
    void Start()
    {
        int personajeLayer = LayerMask.NameToLayer("PersonajeLayer");
        capasIgnoradas = ~(1 << personajeLayer);
        rb = gameObject.GetComponent<Rigidbody>();

        rb.velocity = transform.forward * speed;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
