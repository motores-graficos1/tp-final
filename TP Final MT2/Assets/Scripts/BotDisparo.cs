using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotDisparo : MonoBehaviour
{

    public GameObject enemyfirePoint;
    public GameObject balas;
    void Start()
    {
        InvokeRepeating("DispararBalasEnemigos", 1, 2);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void DispararBalasEnemigos()
    {
        Instantiate(balas, enemyfirePoint.transform.position, enemyfirePoint.transform.rotation);
    }
}
