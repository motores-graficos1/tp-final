using System.Collections;
using System.Collections.Generic;
using Cinemachine;

using UnityEngine;

public class Pistola : MonoBehaviour
{
    public GameObject prefab; // El prefab que quieres lanzar
    public Transform puntoLanzamiento; // El punto de origen del lanzamiento
    public Camera camara; // Referencia a la c�mara principal de Unity

    public float fuerza = 10f; // La fuerza con la que se lanzar� el prefab
    public float intervaloDisparo = 0.3f; // Intervalo de tiempo entre disparos

    private float tiempoUltimoDisparo; // Tiempo en el que se realiz� el �ltimo disparo

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && Time.time - tiempoUltimoDisparo >= intervaloDisparo)
        {
            LanzarPrefab();
            tiempoUltimoDisparo = Time.time;
        }
    }

    void LanzarPrefab()
    {
        Ray rayo = camara.ScreenPointToRay(new Vector3(Screen.width / 2f, Screen.height / 2f, 0f));

        Vector3 direccionDisparo = rayo.direction;

        GameObject nuevoPrefab = Instantiate(prefab, puntoLanzamiento.position, puntoLanzamiento.rotation);
        Rigidbody rb = nuevoPrefab.GetComponent<Rigidbody>();

        if (rb != null)
        {
            rb.isKinematic = false; // Aseg�rate de que el prefab no tenga IsKinematic activado

            rb.AddForce(direccionDisparo.normalized * fuerza, ForceMode.Impulse); // Aplica una fuerza hacia adelante para lanzar el prefab
        }
    }
}
