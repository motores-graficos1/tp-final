using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    public float VelocidadMovimiento = 5.0f;
    public float velocidadRotacion = 200.0f;
    public float rotationSpeed = 5f;
    public Camera mainCamera;
 

    private Animator anim;
    public float x, y;
    public int velCorrer;
    public GameObject Pistola;
  

    private bool isZooming = false;
  

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        anim = GetComponent<Animator>();

       
    }

    // Update is called once per frame
    void Update()
    {
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(x, 0f, y) * VelocidadMovimiento;
        movement = Quaternion.Euler(0f, transform.rotation.eulerAngles.y, 0f) * movement;
        movement *= Time.deltaTime;

        transform.Translate(movement, Space.World);




        anim.SetFloat("VelX", x);
        anim.SetFloat("VelY", y);

        if (Input.GetKey(KeyCode.LeftShift))
        {
            VelocidadMovimiento = velCorrer;
            if (y > 0)
            {
                anim.SetBool("correr", true);
            }
            else
            {
                anim.SetBool("correr", false);
            }
        }
        else
        {
            anim.SetBool("correr", false);
            VelocidadMovimiento = 5.0f;
        }

        if (Input.GetMouseButton(1))
        {
            
            Pistola.SetActive(true);
            anim.SetBool("Arma", true);
        }
        else
        {
            
            Pistola.SetActive(false);
            anim.SetBool("Arma", false);
        }

        if (Input.GetMouseButtonDown(1)) // Clic derecho
        {
            isZooming = true;
        }

        if (Input.GetMouseButtonUp(1)) // Soltar clic derecho
        {
            isZooming = false;
            
        }


        if (Input.GetMouseButton(1) && mainCamera != null)
        {
            // Obtiene la direcci�n desde el personaje hacia la direcci�n de la c�mara
            Vector3 targetDirection = mainCamera.transform.forward;

            // Proyecta la direcci�n al plano horizontal (ejes X y Z)
            targetDirection.y = 0f;
            targetDirection.Normalize();

            // Calcula la rotaci�n necesaria para mirar hacia la direcci�n de la c�mara
            Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);

            // Aplica solo la rotaci�n en el eje Y
            targetRotation.eulerAngles = new Vector3(transform.rotation.eulerAngles.x, targetRotation.eulerAngles.y, transform.rotation.eulerAngles.z);

            // Suaviza la rotaci�n
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
        }

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

       
    }

    

}
