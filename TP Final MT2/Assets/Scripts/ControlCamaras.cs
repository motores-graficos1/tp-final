using System.Collections;
using System.Collections.Generic;
using Cinemachine;

using UnityEngine;

public class ControlCamaras : MonoBehaviour
{
    Vector2 mouseMirar;
    Vector2 suavidadV;

    public float sensibilidad = 5.0f;
    public float suavizado = 2.0f;
    public Camera camaraPrimerapersona;

    //public Camera camaraPrimerapersona;

    GameObject Jugador;
    // Start is called before the first frame update
    void Start()
    {
        Jugador = this.transform.parent.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        md = Vector2.Scale(md, new Vector2(sensibilidad * suavizado, sensibilidad * suavizado));

        suavidadV.x = Mathf.Lerp(suavidadV.x, md.x, 1f / suavizado);

        suavidadV.y = Mathf.Lerp(suavidadV.y, md.y, 1f / suavizado);

        mouseMirar += suavidadV;
        mouseMirar.y = Mathf.Clamp(mouseMirar.y, -90f, 90f);
        transform.localRotation = Quaternion.AngleAxis(-mouseMirar.y, Vector3.right);
        Jugador.transform.localRotation = Quaternion.AngleAxis(mouseMirar.x, Jugador.transform.up);

        Ray rayo = camaraPrimerapersona.ScreenPointToRay(new Vector3(Screen.width / 2f, Screen.height / 2f, 0f));
        RaycastHit hit;

        if (Physics.Raycast(rayo, out hit))
        {
            // Aqu� puedes hacer algo con el objeto golpeado por el rayo
            GameObject objetoGolpeado = hit.collider.gameObject;
            Debug.Log("Objeto golpeado: " + objetoGolpeado.name);
        }
    }
}
