using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBots : MonoBehaviour
{
    private int hp;
    private Rigidbody enemyRb;
    private GameObject Player;
    private GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        hp = 100;
        Player = GameObject.Find("Jugador");
        enemyRb = this.GetComponent<Rigidbody>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    private void Update()
    {
        if (Player != null)
        {
            transform.LookAt(Player.transform);
        }
    }
    // Update is called once per frame
    public void recibirDaņo()
    {
        hp = hp - 25;
        if (hp <= 0)
        {
            gameManager.defeatedEnemies += 1;
            this.desaparecer();
        }
    }

    private void FixedUpdate()
    {
        SeguirJugador(30);
    }

    private void desaparecer()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {
            recibirDaņo();
        }
    }

    private void SeguirJugador(float speed)
    {
        enemyRb.AddForce((Player.transform.position - transform.position).normalized * speed * 10 * Time.deltaTime);
    }
}
